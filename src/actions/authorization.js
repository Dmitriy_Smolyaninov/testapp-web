import base64url from 'base64url';
import cryptoJs from 'crypto-js';
import Axios from 'axios';
import Cookies from 'js-cookie';
export const Successful_Auth = 'Successful_Auth';
export const UnSuccessful_Auth = 'UnSuccessful_Auth';

export function successfulAuth(payload) {
	return {
		type: Successful_Auth,
		payload
	};
}

export function unsuccessfulAuth(payload) {
	return {
		type: UnSuccessful_Auth,
		payload
	};
}

export function PostData(url, user) {
	const header = { alg: 'HS256', typ: 'JWT' };
	const payload = JSON.stringify(user);
	const SECRET_KEY = 'Password';
	const unsignedToken =
		base64url.encode(JSON.stringify(header)) + '.' + base64url.encode(payload);
	const signature = cryptoJs.HmacSHA256(unsignedToken, SECRET_KEY);
	const token =
		base64url.encode(JSON.stringify(header)).toString() +
		'.' +
		base64url.encode(payload).toString() +
		'.' +
		signature.toString();

	return (dispatch) => {
		return new Promise((resolve, reject) => {
			fetch(url, {
				method: 'POST',
				body: JSON.stringify({ token: token }),
				headers: {
					'Content-Type': 'application/json'
				}
			}).then((res) => {
				console.log(res);
				if (!res.ok) reject('Authorization error');
				else {
					Cookies.set('token', token);
					resolve('Authorization was successful');
				}
			});
		});
	};
}

export function ForgotPassword(url, email) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			Axios.post(`http://localhost:8080/test/forgot`, {
				email: email
			})
				.then((response) => {
					resolve(response);
				})
				.catch((error) => {
					reject(error);
				});
		});
	};
}

export function UpdatePassword(url, password) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			Axios.post('http://localhost:8080/test/update', {
				password: password
			})
				.then((response) => {
					resolve(response);
				})
				.catch((error) => {
					reject(error);
				});
		});
	};
}

export function NewUser(url, user) {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			Axios.post(url, {
				email: user.email,
				password: user.password
			})
				.then((response) => {
					resolve(response);
				})
				.catch((error) => {
					reject(error);
				});
		});
	};
}
