var Tasks = [
    {
        id: 0,
        client: 'Client1',
        img:'Client.jpg',
        text:'Suscipit adipiscing bibendum est ultricies integer. Dictum non consectetur a erat nam at. Lacus luctus accumsan tortor posuere. Diam maecenas ultricies mi eget mauris. Faucibus purus in massa tempor nec feugiat nisl pretium.',
        date:'01.10.2019',
        rating:0
    },
    {
        id: 1,
        client: 'Client2',
        img:'Client.jpg',
        text:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt praesent semper feugiat nibh sed pulvinar proin gravida hendrerit. Cursus in hac habitasse platea dictumst.',
        date:'01.10.2019',
        rating:0
    },
    {
        id: 2,
        client: 'Client3',
        img:'Client.jpg',
        text:'Turpis egestas sed tempus urna et. Interdum consectetur libero id faucibus nisl tincidunt. Leo duis ut diam quam nulla porttitor massa id. Felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices.',
        date:'01.10.2019',
        rating:0
    },
    {
        id: 3,
        client: 'Client4',
        img:'Client.jpg',
        text:'Dictum sit amet justo donec enim diam vulputate ut. Sit amet volutpat consequat mauris nunc. Urna molestie at elementum eu facilisis sed odio morbi quis. Curabitur gravida arcu ac tortor dignissim convallis aenean et.',
        date:'01.10.2019',
        rating:0
    }
];

export default Tasks;
