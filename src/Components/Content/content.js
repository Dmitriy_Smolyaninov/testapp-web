import React from 'react';
import { Field, reduxForm } from 'redux-form';
import Navigation from '../Navigation/navigation';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import Active from './Active/active';
import Completed from './Completed/completed';
import Saved from './Saved/saved';
import Cookie from 'js-cookie';
import base64url from 'base64url';
import './content.scss';
import { connect } from 'react-redux';

class Content extends React.Component {



	constructor() {
		super();
		let token = Cookie.get('token');
		let tokenArr = token.split('.');
		let email = base64url.decode(tokenArr[1]);
		this.state = {
			email: JSON.parse(email).email
		};

		console.log(JSON.parse(email).email);
	}

	render() {
		return (
			<section className="Content">
				<Navigation />
				<div className="Window">
					<div className="Email">
						<h2>{this.state.email}</h2>
						<button>Exit</button>
					</div>
					<Switch>
						<Route path="/Content/Active" component={Active} />
						<Route path="/Content/Completed" component={Completed} />
						<Route path="/Content/Saved" component={Saved} />
					</Switch>
				</div>
			</section>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state
	};
}

// export default Content;
export default connect(mapStateToProps)(Content);
