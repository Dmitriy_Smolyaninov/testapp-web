import React from 'react';
import image from '../images/Client.jpg';
import './active.scss';
import tasks from '../Tasks'
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

class Active extends React.Component {
	render() {
		return (
			<section className="Active">
				<div className="Search">
					<p>Search:</p>
					<input type="text" />
				</div>

				<div className="Items">
				{tasks.map(task => {
					return (
						<div className="Item" key={task.id}>
							<div className="Image">
								<img src={image} alt="Client" />
								<p>{task.client}</p>
							</div>
							<div className="Text">
								<p>{task.text}</p>
							</div>
							<div className="Info">
								<button>Continue</button>
								<p>{task.date}</p>
								<button>Save</button>
							</div>
						</div>
					)
				})}
				</div>
			</section>
		);
	}
}

export default Active;
