import React from 'react';
import image from '../images/Client.jpg';
import './saved.scss';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

class Saved extends React.Component {
	render() {
		return (
			<section className="Active">
				<div className="Search">
					<p>Search:</p>
					<input type="text" />
				</div>

				<div className="Items">
					<div className="Item">
						<div className="Image">
							<img src={image} alt="Client" />
							<p>Client 1</p>
						</div>
						<div className="Text">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
								eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam
								maecenas ultricies mi eget mauris pharetra et.
							</p>
						</div>
						<div className="Info">
							<button>Continue</button>
							<p>12 days ago</p>
							<p>Rating</p>
							<div>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
							</div>
							<button>Delete</button>
						</div>
					</div>
					<div className="Item">
						<div className="Image">
							<img src={image} alt="Client" />
							<p>Client 1</p>
						</div>
						<div className="Text">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
								eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam
								maecenas ultricies mi eget mauris pharetra et.
							</p>
						</div>
						<div className="Info">
							<button>Continue</button>
							<p>12 days ago</p>
							<p>Rating</p>
							<div>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
							</div>
							<button>Delete</button>
						</div>
					</div>
					<div className="Item">
						<div className="Image">
							<img src={image} alt="Client" />
							<p>Client 1</p>
						</div>
						<div className="Text">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
								eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam
								maecenas ultricies mi eget mauris pharetra et.
							</p>
						</div>
						<div className="Info">
							<button>Continue</button>
							<p>12 days ago</p>
							<p>Rating</p>
							<div>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
								<i className="fas fa-star"></i>
							</div>
							<button>Delete</button>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default Saved;
