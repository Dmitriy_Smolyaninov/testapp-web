import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import './forgot.scss';
import { ForgotPassword } from '../../actions/authorization';
import { ToastContainer, toast } from 'react-toastify';
import base64url from 'base64url';
import cryptoJs from 'crypto-js';
import { NavLink, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const required = (value) => (value ? undefined : 'Заполните поле');

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div className="inputField">
		<label>{label}</label>
		<div>
			<Input {...input} placeholder={label} type={type} />
			{touched &&
				(error && (
					<span>
						{' '}
						<i className="fas fa-exclamation"></i> {error}
					</span>
				))}
		</div>
	</div>
);

class Forgot extends React.Component {
	constructor() {
		super();
		this.state = {
			flag: false,
			link: ''
		};
	}

	render() {
		const { handleSubmit } = this.props;

		const submit = (values) => {
			this.props
				.forgotPassword('http://localhost:8080/test/forgot', values.email)
				.then((result) => {
					console.log(result.data);
					this.setState({
						flag: true,
						link: `/Update/${values.email}/${result.data}`
					});
				})
				.catch((error) => console.log(error));
		};

		const navigation = () => {
			this.props.history.push(this.state.link);
		};

		return (
			<section>
				<h1>Forgot password</h1>
				<form onSubmit={handleSubmit(submit)} className="form">
					<Field
						component={renderField}
						name="email"
						label="Enter email"
						type="email"
						validate={required}
					/>

					<div>
						{!this.state.flag && (
							<Button color="primary" type="submit">
								Send recovery link
							</Button>
						)}
						{this.state.flag && (
							<Button color="primary" onClick={navigation}>
								Update password
							</Button>
						)}
					</div>
					<div className="links">
						<NavLink tag={Link} to="/">
							Authorization
						</NavLink>
						<NavLink tag={Link} to="/Registration">
							Registration
						</NavLink>
					</div>
				</form>
			</section>
		);
	}
}

Forgot = reduxForm({
	form: 'form'
})(Forgot);

function mapStateToProps(state) {
	return {
		user: state
	};
}

const ForgotPasswordToProps = (dispatch) => {
	return {
		forgotPassword: (url, user) => dispatch(ForgotPassword(url, user))
	};
};

export default connect(
	mapStateToProps,
	ForgotPasswordToProps
)(Forgot);
