import React from 'react';
import { Field, reduxForm } from 'redux-form';
//import './vkontakte.scss';
import { NavLink, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const required = (value) => (value ? undefined : 'Заполните поле');

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div className="inputField">
		<label>{label}</label>
		<div>
			<Input {...input} placeholder={label} type={type} />
			{touched &&
				(error && (
					<span>
						{' '}
						<i className="fas fa-exclamation"></i> {error}
					</span>
				))}
		</div>
	</div>
);

class Vkontakte extends React.Component {
	render() {
		const { handleSubmit } = this.props;

		const submit = (values) => {};

		return (
			<section>
				<h1>Vkontakte</h1>
				<form onSubmit={handleSubmit(submit)} className="form">
					<Field
						component={renderField}
						name="email"
						label="Enter email"
						type="email"
						validate={required}
					/>
					<Field
						component={renderField}
						name="Password"
						label="Enter Password"
						type="password"
						validate={required}
					/>
					<div>
						<Button color="primary" type="submit">
							Registration
						</Button>
					</div>
					<div className="links">
						<NavLink tag={Link} to="/">
							Authorization
						</NavLink>
					</div>
				</form>
			</section>
		);
	}
}

Vkontakte = reduxForm({
	form: 'form'
})(Vkontakte);

export default Vkontakte;
