import React from 'react';
import { Field, reduxForm } from 'redux-form';
import './google.scss';
import { NavLink, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const required = (value) => (value ? undefined : 'Заполните поле');

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div className="inputField">
		<label>{label}</label>
		<div>
			<Input {...input} placeholder={label} type={type} />
			{touched &&
				(error && (
					<span>
						{' '}
						<i className="fas fa-exclamation"></i> {error}
					</span>
				))}
		</div>
	</div>
);

class Google extends React.Component {
	render() {
		const { handleSubmit } = this.props;

		const submit = (values) => {};

		return (
			<section>
				<h1>Google</h1>
				<form onSubmit={handleSubmit(submit)} className="form">
					<Field
						component={renderField}
						name="email"
						label="Enter email"
						type="email"
						validate={required}
					/>
					<Field
						component={renderField}
						name="Password"
						label="Enter Password"
						type="password"
						validate={required}
					/>
					<div>
						<Button color="primary" type="submit">
							Registration
						</Button>
					</div>
					<div className="buttons">
						<NavLink tag={Link} to="/">
							Authorization
						</NavLink>
					</div>
				</form>
			</section>
		);
	}
}

Google = reduxForm({
	form: 'form'
})(Google);

export default Google;
