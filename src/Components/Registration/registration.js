import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { NewUser, PostData } from '../../actions/authorization';
import './registration.scss';
import { connect } from 'react-redux';
import { toast, ToastContainer } from 'react-toastify';
import { NavLink, Input, Button } from 'reactstrap';

const required = (value) => (value ? undefined : 'Заполните поле');
const password = (value) =>
	value && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/i.test(value)
		? undefined
		: 'Password error';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div className="inputField">
		<label>{label}</label>
		<div>
			<Input {...input} placeholder={label} type={type} />
			{touched &&
				(error && (
					<span>
						{' '}
						<i className="fas fa-exclamation"></i> {error}
					</span>
				))}
		</div>
	</div>
);

class Registration extends React.Component {
	successful = (info) =>
		toast.info(info, {
			position: 'top-right',
			autoClose: 5000,
			hideProgressBar: true,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});

	unsuccessful = (info) =>
		toast.error(info, {
			position: 'top-right',
			autoClose: 5000,
			hideProgressBar: true,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});

	render() {
		const { handleSubmit } = this.props;

		const submit = (values) => {
			if (values.Password == values.RepeatPassword) {
				this.successful('Create new user');
				setTimeout(() => {
					this.props.history.push('./Content');
				}, 1000);
			} else {
				this.unsuccessful('Different passwords');
			}
		};

		return (
			<section>
				<ToastContainer />
				<h1>Registration</h1>
				<form onSubmit={handleSubmit(submit)} className="form">
					<Field
						component={renderField}
						name="email"
						label="Enter email"
						type="email"
						validate={required}
					/>
					<Field
						component={renderField}
						name="Password"
						label="Enter Password"
						type="password"
						validate={[required, password]}
					/>
					<Field
						component={renderField}
						name="RepeatPassword"
						label="Repeat Password"
						type="password"
						validate={[required, password]}
					/>
					<div>
						<Button color="primary" type="submit">
							Registration
						</Button>
					</div>
					<div className="links">
						<NavLink tag={Link} to="/">
							Authorization
						</NavLink>
					</div>
				</form>
			</section>
		);
	}
}

Registration = reduxForm({
	form: 'form'
})(Registration);

const NewUserToProps = (dispatch) => {
	return {
		newUser: (url, user) => dispatch(NewUser(url, user))
	};
};

export default connect(NewUserToProps)(Registration);
