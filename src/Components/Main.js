import React from 'react';
import {
	Switch,
	Route,
	Redirect,
	Router,
	BrowserRouter
} from 'react-router-dom';
import './Styles/Form.scss';
import Authorization from './Authorization/authorization';
import Registration from './Registration/registration';
import Forgot from './Forgot/forgot';
import Update from './Update/update';
import Vkontakte from './Vkontakte/vkontakte';
import Google from './Google/google';
import Content from './Content/content';

const Main = () => (
	<main>
		<Switch>
			<Route exact path="/" component={Authorization} />
			<Route exact path="/Registration" component={Registration} />
			<Route exact path="/Forgot" component={Forgot} />
			<Route exact path="/Update/:email/:token" component={Update} />
			<Route exact path="/Vkontakte" component={Vkontakte} />
			<Route exact path="/Google" component={Google} />
			<Route exact path="/Content" component={Content} />
			<Route exact path="/Content/:Window" component={Content} />
		</Switch>
	</main>
);

export default Main;
