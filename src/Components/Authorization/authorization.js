import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import reduce from '../../reducers/reduceAuth';
import {
	PostData,
	successfulAuth,
	unsuccessfulAuth
} from '../../actions/authorization';
import ClassNames from 'classnames';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import { NavLink, Input, Button } from 'reactstrap';
import 'react-toastify/dist/ReactToastify.css';
import './authorization.scss';

let cx = ClassNames('.buttons', '.inputField');

const required = (value) => (value ? undefined : 'Заполните поле');
const password = (value) =>
	value && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/i.test(value)
		? undefined
		: 'Password error';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div className="inputField">
		<label>{label}</label>
		<div>
			<Input {...input} placeholder={label} type={type} />
			{touched &&
				(error && (
					<span>
						{' '}
						<i className="fas fa-exclamation"></i> {error}
					</span>
				))}
		</div>
	</div>
);

class Authorization extends React.Component {
	successful = (info) =>
		toast.info(info, {
			position: 'top-right',
			autoClose: 5000,
			hideProgressBar: true,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});

	unsuccessful = (info) =>
		toast.error(info, {
			position: 'top-right',
			autoClose: 5000,
			hideProgressBar: true,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});

	render() {
		//console.log('***render***');
		//console.log('status', this.props.user.reduce);
		//console.log(store.getState());
		//this.navigate();
		const { handleSubmit } = this.props;

		const submit = (values) => {
			this.props
				.postData('http://localhost:8080/test', {
					email: values.email,
					password: values.password
				})
				.then((result) => {
					// console.log(this.props);
					this.props.dispatch(
						successfulAuth({ status: result, email: values.email })
					);
					console.log('status', this.props.user.reduce);
					this.setState({ error: null });
					this.successful(this.props.user.reduce.status);
					this.props.history.push('./Content');
				})
				.catch((error) => {
					console.log(error);
					this.props.dispatch(unsuccessfulAuth(error));
					console.log('status', this.props.user.reduce);
					this.setState({ error: 'Data entered incorrectly' });
					this.unsuccessful(this.props.user.reduce.status);
				});
		};

		return (
			<section>
				<ToastContainer />
				<h1>Authorization</h1>
				<form onSubmit={handleSubmit(submit)} className="form">
					<Field
						component={renderField}
						name="email"
						label="Enter email"
						type="email"
						validate={required}
					/>
					<Field
						component={renderField}
						name="password"
						label="Enter password"
						type="password"
						validate={[required, password]}
					/>
					<div>
						<Button color="primary" type="submit">
							Submit
						</Button>
					</div>
					<div className="SocialNetworks">
						<div>
							<i className="fab fa-vk"></i>
							<NavLink tag={Link} to="/Vkontakte">
								enter trough VK
							</NavLink>
						</div>
						<div>
							<i className="fab fa-google"></i>
							<NavLink tag={Link} to="/Google">
								enter trough Google
							</NavLink>
						</div>
					</div>
					<div className="links">
						<NavLink tag={Link} to="/Registration">
							Registration
						</NavLink>
						<NavLink tag={Link} to="/Forgot">
							Forgot password?
						</NavLink>
					</div>
				</form>
			</section>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state
	};
}

const PostDataToProps = (dispatch) => {
	return {
		postData: (url, user) => dispatch(PostData(url, user))
	};
};

Authorization = reduxForm({
	form: 'form'
})(Authorization);

Authorization.contextTypes = {
	router: PropTypes.object
};

//export default Authorization;
export default connect(
	mapStateToProps,
	PostDataToProps
)(Authorization);
