import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Authorization from './authorization';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('Search component', () => {
	test('renders', () => {
		const wrapper = shallow(<Authorization />);
		expect(wrapper.exists()).toBe(true);
	});
});
