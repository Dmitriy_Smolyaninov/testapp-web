import React from 'react';
import PropTypes from 'prop-types';
import { createStore, combineReducers } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import base64url from 'base64url';
import cryptoJs from 'crypto-js';
import { UpdatePassword } from '../../actions/authorization';
import { toast, ToastContainer } from 'react-toastify';
import { NavLink, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
//import './update.scss';

const required = (value) => (value ? undefined : 'Заполните поле');
const password = (value) =>
	value && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/i.test(value)
		? undefined
		: 'Password error';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div className="inputField">
		<label>{label}</label>
		<div>
			<Input {...input} placeholder={label} type={type} />
			{touched &&
				(error && (
					<span>
						{' '}
						<i className="fas fa-exclamation"></i> {error}
					</span>
				))}
		</div>
	</div>
);

class Update extends React.Component {
	successful = (info) =>
		toast.info(info, {
			position: 'top-right',
			autoClose: 5000,
			hideProgressBar: true,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});

	unsuccessful = (info) =>
		toast.error(info, {
			position: 'top-right',
			autoClose: 5000,
			hideProgressBar: true,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});

	render() {
		const { handleSubmit } = this.props;

		const submit = (values) => {
			if (values.password == values.repeatPassword) {
				this.props
					.updatePassword('http://localhost:8080/test/update', values.password)
					.then((result) => {
						this.successful(result.data);
						setTimeout(() => {
							this.props.history.push('/');
						}, 500);
					})
					.catch((error) => console.log('error', error));
			} else {
				this.unsuccessful('Different passwords');
			}
		};

		let date = new Date();
		const SECRET_KEY =
			this.props.match.params.email.toString() + date.getHours();
		const tokenArr = this.props.match.params.token.split('.');
		const header = tokenArr[0];
		const body = tokenArr[1];
		const signature = tokenArr[2];

		const headerDecode = base64url.decode(header);
		const bodyDecode = base64url.decode(body);
		const unsignedToken =
			base64url.encode(headerDecode) + '.' + base64url.encode(bodyDecode);
		const signatureNew = cryptoJs.HmacSHA256(unsignedToken, SECRET_KEY);

		if (signature == signatureNew) {
			return (
				<section>
					<ToastContainer />
					<h1>Update password</h1>
					<form onSubmit={handleSubmit(submit)} className="form">
						<Field
							component={renderField}
							name="password"
							label="Enter Password"
							type="password"
							validate={[required, password]}
						/>
						<Field
							component={renderField}
							name="repeatPassword"
							label="Repeat Password"
							type="password"
							validate={[required, password]}
						/>
						<div>
							<Button color="primary" type="submit">
								Update password
							</Button>
						</div>
						<div className="links">
							<NavLink tag={Link} to="/">
								Authorization
							</NavLink>
							<NavLink tag={Link} to="/Registration">
								Registration
							</NavLink>
						</div>
					</form>
				</section>
			);
		}

		//console.log(this.props.match.params);
	}
}

Update = reduxForm({
	form: 'form'
})(Update);

function mapStateToProps(state) {
	return {
		user: state
	};
}

const UpdatePasswordToProps = (dispatch) => {
	return {
		updatePassword: (url, user) => dispatch(UpdatePassword(url, user))
	};
};

export default connect(
	mapStateToProps,
	UpdatePasswordToProps
)(Update);

// export default Update;
