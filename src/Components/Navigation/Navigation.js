import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import './navigation.scss';

class Navigation extends React.Component {
	render() {
		//console.log(this.props);
		//this.props.history.push('./Content/Action');

		return (
			<section className="Navigation">
				<ul>
					<li>
						<Link to="/Content/Active">Active</Link>
					</li>
					<li>
						<Link to="/Content/Completed">Completed</Link>
					</li>
					<li>
						<Link to="/Content/Saved">Saved</Link>
					</li>
				</ul>
			</section>
		);
	}
}

Navigation = reduxForm({
	form: 'form'
})(Navigation);

export default Navigation;
