import React from 'react';
import './App.css';
import Main from './components/Main';
import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import reduce from './reducers/reduceAuth';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';
import { BrowserRouter as Router, Route } from 'react-router-dom';
require('dotenv').config();

let reducers = combineReducers({ reduce, form: formReducer });

let store = createStore(reducers, applyMiddleware(thunk));

class App extends React.Component {
	render() {

		console.log(process.env);

		return (
			<Provider store={store}>
				<Main />
			</Provider>
		);
	}
}

export default App;
