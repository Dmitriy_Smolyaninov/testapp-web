import { Successful_Auth, UnSuccessful_Auth } from '../actions/authorization';

let initialState = {
	email: 'email',
	password: 'password',
	status: ''
};

function authorization(state = initialState, action) {
	switch (action.type) {
		case Successful_Auth:
			// console.log('successful reduce');
			// console.log(action);
			return {
				...state,
				email: action.payload.email,
				status: action.payload.status
			};

		case UnSuccessful_Auth:
			// console.log('unsuccessful reduce');
			// console.log(action);
			return {
				...state,
				status: action.payload
			};

		default:
			return state;
	}
}

export default authorization;
